angular.module('starter.controllers')
.controller('InfiniteScrollCtrl', ['$scope','$timeout', function($scope, $timeout) {

  var self = this;

  self.items = [];
  self.limit = 15;
  self.offset = 0;
  self.moreDataCanBeLoaded = true;

  var itemPool = []

  for (var i = 0; i < 100; i++) {
    itemPool.push("item " + i);
  }

  self.loadMoreItems = function() {
    $timeout(function() {
      var newSlice = itemPool.slice(self.offset, self.offset + self.limit);
      self.items = self.items.concat(newSlice);
      self.offset = self.offset + self.limit;
      self.moreDataCanBeLoaded = newSlice.length === self.limit;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, 1000);
  }

}]);