// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter.controllers', ['ionic']);

angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.constant('$ionicLoadingConfig', {
  templateUrl: 'templates/_loading.html'
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    //controller: 'AppCtrl'
  })

  .state('app.welcome', {
    url: "/welcome",
    views: {
      'menuContent': {
        templateUrl: "templates/welcome.html"
      }
    }
  })

  .state('app.action_sheet', {
    url: "/action_sheet",
    views: {
      'menuContent': {
        templateUrl: "templates/action_sheet.html",
        controller: "ActionSheetCtrl as ctrl"
      }
    }
  })

  .state('app.backdrop', {
    url: "/backdrop",
    views: {
      'menuContent': {
        templateUrl: "templates/backdrop.html",
        controller: "BackdropCtrl as ctrl"
      }
    }
  })

  .state('app.content', {
    url: "/content",
    views: {
      'menuContent': {
        templateUrl: "templates/content.html",
        controller: "ContentCtrl as ctrl"
      }
    }
  })

  .state('app.form_input', {
    url: "/form_input",
    views: {
      'menuContent': {
        templateUrl: "templates/form_input.html",
        controller: "FormInputCtrl as ctrl"
      }
    }
  })

  .state('app.list', {
    url: "/list",
    views: {
      'menuContent': {
        templateUrl: "templates/list.html",
        controller: "ListCtrl as ctrl"
      }
    }
  })

  .state('app.loading', {
    url: "/loading",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/loading.html",
        controller: "LoadingCtrl as ctrl"
      }
    }
  })

  .state('app.modal', {
    url: "/modal",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/modal.html",
        controller: "ModalCtrl as ctrl"
      }
    }
  })

  .state('app.infinite_scroll', {
    url: "/infinite_scroll",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "templates/infinite_scroll.html",
        controller: "InfiniteScrollCtrl as ctrl"
      }
    }
  })

  .state('app.dummy', {
    url: "/dummy",
    views: {
      'menuContent': {
        templateUrl: "templates/dummy.html"
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/welcome');
});
