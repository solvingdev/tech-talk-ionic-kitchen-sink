angular.module('starter.controllers')
.controller("ActionSheetCtrl", ["$ionicActionSheet", "$timeout", function($ionicActionSheet, $timeout) {
  var self = this;

  self.showDanger = false;

  self.show = function() {

    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
       { text: '<b>Condividi</b> questo' },
       { text: 'Sposta' }
      ],
      destructiveText: 'Cancella (Danger zone!)',
      titleText: 'Cosa vuoi fare?',
      cancelText: 'Annulla',
      cancel: function() {
      
      },
      buttonClicked: function(index) {
        switch(index) {
          case 0:
            alert("Condiviso");
            break;
          case 1:
            alert("Spostato");
            break;
        }
       return true;
      },
      destructiveButtonClicked: function() {
        var userchoice = confirm("Qualcosa di pericoloso sta per accadere");
        self.showDanger = userchoice;

        $timeout(function() {
          self.showDanger = false;
        }, 2500);
        
        return userchoice;
      }
    });
  };

}]);