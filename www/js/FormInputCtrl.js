angular.module('starter.controllers')
.controller('FormInputCtrl', [function() {
  var self = this;

  self.heroes = [
    {name:"Iron Man", checked: false},
    {name:"The Hulk", checked: false},
    {name:"Aquaman", checked: false}
  ];

  self.villains = [
    {id:"TKMT", name:"Taskmaster"},
    {id:"ULTR", name:"Ultron"},
    {id:"LK", name:"Loki"}
  ];

  self.environmentSettings = [
    {name: "Radiazioni", checked: false},
    {name: "Pioggia", checked: false},
    {name: "Civili", checked: false},
  ]


}])