angular.module('starter.controllers')
.controller('BackdropCtrl', ['$ionicBackdrop', '$timeout', function($ionicBackdrop, $timeout) {
  var self = this;

  self.action = function() {
    $ionicBackdrop.retain();
    $timeout(function(){
      $ionicBackdrop.release();
    }, 1000);
  }
}]);