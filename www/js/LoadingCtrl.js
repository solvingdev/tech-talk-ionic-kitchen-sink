angular.module('starter.controllers')
.controller('LoadingCtrl', ['$ionicLoading', '$timeout', function($ionicLoading, $timeout) {
  var self = this;
  $ionicLoading.show();

  $timeout(function() {
    $ionicLoading.hide();
    self.showImage = true;
  }, 2000);
}]);