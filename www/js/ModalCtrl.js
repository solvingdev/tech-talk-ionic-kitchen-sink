angular.module('starter.controllers')
.controller('ModalCtrl', ['$scope', '$ionicModal', function($scope, $ionicModal) {
  var self = this;

  $ionicModal.fromTemplateUrl('test-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    self.modal = modal;
  });

  self.openModal = function() {
    self.modal.show();
  };

  self.closeModal = function() {
    self.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    console.log("modale distrutto");
    self.modal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
    console.log("modale nascosto");
  });

  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
    console.log("modale rimosso");
  });
}])